package restserveragent

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sync"
	"time"

	rad "gitlab.utc.fr/ia041/ia04-vote/agt"
	"gitlab.utc.fr/ia041/ia04-vote/comsoc"
)

type RestServerAgent struct {
	sync.Mutex
	ID     int
	addr   string
	ballot []BallotAgent
}

type BallotAgent struct {
	ID       int
	profile  comsoc.Profile
	rule     string
	voterIDs []string
	options  []int
	count    comsoc.Count
	bestAlts []comsoc.Alternative
	isClosed bool
	votedMap map[string]int
	deadline time.Time
}

func NewRestServerAgent(addr string) *RestServerAgent {
	ballot := make([]BallotAgent, 1)
	return &RestServerAgent{
		ID:     0,
		addr:   addr,
		ballot: ballot,
	}
}

// Test de la méthode
func (rsa *RestServerAgent) checkMethod(method string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != method {
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, "method '%q' not allowed.", r.Method)
		return false
	}
	return true
}

func (*RestServerAgent) decodeRequestNewBallot(r *http.Request) (req rad.RequestNewBallot, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (*RestServerAgent) decodeRequestVote(r *http.Request) (req rad.RequestVote, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (*RestServerAgent) decodeRequestResult(r *http.Request) (req rad.RequestResult, err error) {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)
	err = json.Unmarshal(buf.Bytes(), &req)
	return
}

func (rsa *RestServerAgent) doNewBallot(w http.ResponseWriter, r *http.Request) {
	// mise à jour du nombre de requêtes
	rsa.Lock()
	defer rsa.Unlock()

	log.Println("Received a request of new_ballot.") //ok

	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	req, err := rsa.decodeRequestNewBallot(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	// traitement de la requête
	var resp rad.ResponseNewBallot

	rsa.ID += 1
	var newBallot BallotAgent
	rsa.ballot = append(rsa.ballot, newBallot)
	rsa.ballot[rsa.ID].ID = rsa.ID
	rsa.ballot[rsa.ID].rule = req.Rule
	rsa.ballot[rsa.ID].voterIDs = req.VoterIDs
	profile := make(comsoc.Profile, 0)
	bestAlts := make([]comsoc.Alternative, 0)
	votedMap := make(map[string]int)
	rsa.ballot[rsa.ID].profile = profile
	rsa.ballot[rsa.ID].bestAlts = bestAlts
	rsa.ballot[rsa.ID].votedMap = votedMap

	if req.Rule != "approval" && req.Rule != "borda" && req.Rule != "condorcet" && req.Rule != "copeland" && req.Rule != "majority" && req.Rule != "majority2tours" && req.Rule != "stv" {
		w.WriteHeader(http.StatusNotImplemented)
		log.Println("the " + req.Rule + " rule is not implemented.") //OK
		msg := fmt.Sprintf("Rule '%s' is not implemented.", req.Rule)
		w.Write([]byte(msg))
		return
	}

	deadline, err := time.Parse("Mon Jan _2 15:04:05 UTC 2006", req.Deadline)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Println("Deadline format error.") //OK
		msg := fmt.Sprintf("Deadline '%s' is not in a correct format.", req.Deadline)
		w.Write([]byte(msg))
		return
	}
	rsa.ballot[rsa.ID].deadline = deadline

	resp.BallotID = fmt.Sprintf("vote%d", rsa.ballot[rsa.ID].ID)

	w.WriteHeader(http.StatusCreated)
	log.Println("The ballot " + resp.BallotID + " is created, whose rule is " + rsa.ballot[rsa.ID].rule) //ok
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}

func (rsa *RestServerAgent) doVote(w http.ResponseWriter, r *http.Request) {
	// mise à jour du nombre de requêtes
	rsa.Lock()
	defer rsa.Unlock()

	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	req, err := rsa.decodeRequestVote(r)
	if err != nil {
		msg := fmt.Sprintf("DecodeRequestVote Error")
		w.Write([]byte(msg))
		return
	}
	log.Println("The ballot " + req.VoteID + " received a request of vote from " + req.AgentID) //ok
	var BallotID int = 0
	for i := 1; i <= rsa.ID; i++ {
		if fmt.Sprintf("vote%d", rsa.ID) == req.VoteID {
			BallotID = i
		}
	}

	if BallotID == 0 {
		w.WriteHeader(http.StatusNotFound)
		log.Println("The ballot " + req.VoteID + " not found.") //OK
		msg := fmt.Sprintf("The ballot '%s' not found ", req.VoteID)
		w.Write([]byte(msg))
		return
	}

	timeNow := time.Now().Format("Mon Jan _2 15:04:05 UTC 2006")
	timenow, _ := time.Parse("Mon Jan _2 15:04:05 UTC 2006", timeNow)

	if rsa.ballot[BallotID].deadline.Before(timenow) {
		rsa.ballot[BallotID].isClosed = true
		log.Println("The ballot " + req.VoteID + " has closed.")
	}

	if rsa.ballot[BallotID].isClosed {
		w.WriteHeader(http.StatusServiceUnavailable)
		log.Println("The ballot " + req.VoteID + " has closed, votes cannot be counted.")
		msg := fmt.Sprintf("The ballot '%s' is closed, votes cannot be counted", req.VoteID)
		w.Write([]byte(msg))
		return
	}

	if rsa.ballot[BallotID].votedMap[req.AgentID] != 0 {
		w.WriteHeader(http.StatusForbidden)
		log.Println(req.AgentID + " has already voted.")
		msg := fmt.Sprintf("The voter '%s' has already voted", req.AgentID)
		w.Write([]byte(msg))
		return
	}

	if len(req.Options) != 1 && rsa.ballot[BallotID].rule == "approval" {
		w.WriteHeader(http.StatusNotImplemented)
		log.Println("Rule" + rsa.ballot[BallotID].rule + " need options.")
		msg := fmt.Sprintf("Rule '%s' need options (example: 'options: [3]' in5 JSON).", rsa.ballot[BallotID].rule)
		w.Write([]byte(msg))
		return
	}

	if len(req.Options) != 0 && rsa.ballot[BallotID].rule != "approval" {
		w.WriteHeader(http.StatusNotImplemented)
		log.Println("Rule " + rsa.ballot[BallotID].rule + " doesn't need options.")
		msg := fmt.Sprintf("Rule '%s' doesn't need options (example: 'options: []' in JSON).", rsa.ballot[BallotID].rule)
		w.Write([]byte(msg))
		return
	}

	if rsa.ballot[BallotID].rule == "approval" {
		rsa.ballot[BallotID].options = append(rsa.ballot[BallotID].options, int(req.Options[0]))
	}

	// Check if the list of preference is in accordance with the profile
	if rsa.ballot[BallotID].profile != nil && comsoc.CheckProfileAlternative(rsa.ballot[BallotID].profile, req.Prefs) != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Println("Preferences of voter " + req.AgentID + " are not correct in the candidates profile.")
		msg := fmt.Sprintf("Preferences of voter '%s' are not correct in the candidates profile.", req.AgentID)
		w.Write([]byte(msg))
		return
	}

	rsa.ballot[BallotID].profile = append(rsa.ballot[BallotID].profile, req.Prefs)
	rsa.ballot[BallotID].votedMap[req.AgentID] = 1

	w.WriteHeader(http.StatusOK)
	log.Println("The vote of " + req.AgentID + " has been taken in account.") //ok
	msg := fmt.Sprintf("'%s''s vote has been taken in account.", req.AgentID)
	w.Write([]byte(msg))
}

func (rsa *RestServerAgent) doResult(w http.ResponseWriter, r *http.Request) {
	rsa.Lock()
	defer rsa.Unlock()

	// vérification de la méthode de la requête
	if !rsa.checkMethod("POST", w, r) {
		return
	}

	// décodage de la requête
	req, err := rsa.decodeRequestResult(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	var BallotID int = 0
	for i := 1; i <= rsa.ID; i++ {
		if fmt.Sprintf("vote%d", rsa.ID) == req.BallotID {
			BallotID = i
		}
	}

	if BallotID == 0 {
		w.WriteHeader(http.StatusNotFound)
		log.Println("The ballot " + req.BallotID + " not found.")
		msg := fmt.Sprintf("The ballot '%s' not found ", req.BallotID)
		w.Write([]byte(msg))
		return
	}
	log.Println("The ballot " + req.BallotID + " received a request of getting result.")
	timeNow := time.Now().Format("Mon Jan _2 15:04:05 UTC 2006")
	timenow, _ := time.Parse("Mon Jan _2 15:04:05 UTC 2006", timeNow)
	if rsa.ballot[BallotID].deadline.Before(timenow) {
		rsa.ballot[BallotID].isClosed = true
	}

	// Check if it is too early to publish the results (deadline still not met)
	var err1, err2 error
	if !rsa.ballot[BallotID].isClosed {
		w.WriteHeader(http.StatusTooEarly)
		log.Println("The ballot " + req.BallotID + " is still open, result will be available past the set deadline.")
		msg := fmt.Sprintf("The ballot '%s' is still open, result will be available past the set deadline", req.BallotID)
		w.Write([]byte(msg))
		return
	} else {
		switch rsa.ballot[BallotID].rule {
		case "approval":
			rsa.ballot[BallotID].count, err1 = comsoc.ApprovalSWF(rsa.ballot[BallotID].profile, rsa.ballot[BallotID].options)
			rsa.ballot[BallotID].bestAlts, err2 = comsoc.ApprovalSCF(rsa.ballot[BallotID].profile, rsa.ballot[BallotID].options)
		case "borda":
			rsa.ballot[BallotID].count, err1 = comsoc.BordaSWF(rsa.ballot[BallotID].profile)
			rsa.ballot[BallotID].bestAlts, err2 = comsoc.BordaSCF(rsa.ballot[BallotID].profile)
		case "condorcet":
			rsa.ballot[BallotID].bestAlts, err2 = comsoc.CondorcetWinner(rsa.ballot[BallotID].profile)
		case "copeland":
			rsa.ballot[BallotID].count, err1 = comsoc.CopelandSWF(rsa.ballot[BallotID].profile)
			rsa.ballot[BallotID].bestAlts, err2 = comsoc.CopelandSCF(rsa.ballot[BallotID].profile)
		case "majority":
			rsa.ballot[BallotID].count, err1 = comsoc.MajoritySWF(rsa.ballot[BallotID].profile)
			rsa.ballot[BallotID].bestAlts, err2 = comsoc.MajoritySCF(rsa.ballot[BallotID].profile)
		case "majority2tours":
			rsa.ballot[BallotID].count, err1 = comsoc.Majority2ToursSWF(rsa.ballot[BallotID].profile)
			rsa.ballot[BallotID].bestAlts, err2 = comsoc.Majority2ToursSCF(rsa.ballot[BallotID].profile)
		case "stv":
			rsa.ballot[BallotID].bestAlts, err2 = comsoc.STV_SCF(rsa.ballot[BallotID].profile)
		default:
			w.WriteHeader(http.StatusNotImplemented)
			log.Println("Rule" + rsa.ballot[BallotID].rule + " is not implemented.")
			msg := fmt.Sprintf("Rule '%s' is not implemented", rsa.ballot[BallotID].rule)
			w.Write([]byte(msg))
			return
		}
		if err1 != nil || err2 != nil {
			w.WriteHeader(http.StatusInternalServerError)
			msg := fmt.Sprintf("Vote method error.")
			w.Write([]byte(msg))
			return
		}
	}

	// traitement de la requête
	var resp rad.ResponseResult

	resp.Winner = rsa.ballot[BallotID].bestAlts[0]
	resp.Ranking = comsoc.Ranking(rsa.ballot[BallotID].count)
	fmt.Println("The winner is: ", resp.Winner)
	fmt.Println("The ranking is: ", resp.Ranking)
	w.WriteHeader(http.StatusOK)
	serial, _ := json.Marshal(resp)
	w.Write(serial)
}

func (rsa *RestServerAgent) Start() {
	mux := http.NewServeMux()
	mux.HandleFunc("/new_ballot", rsa.doNewBallot)
	mux.HandleFunc("/vote", rsa.doVote)
	mux.HandleFunc("/result", rsa.doResult)

	// création du serveur http
	s := &http.Server{
		Addr:           rsa.addr,
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20}

	// lancement du serveur
	log.Println("Listening on", rsa.addr)
	go log.Fatal(s.ListenAndServe())
}
