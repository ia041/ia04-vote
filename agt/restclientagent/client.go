package restclientagent

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"

	rad "gitlab.utc.fr/ia041/ia04-vote/agt"
	"gitlab.utc.fr/ia041/ia04-vote/comsoc"
)

type Voter struct {
	ag_id   string
	vote_id string
	url     string
	prefs   []comsoc.Alternative
	options []int
}

func NewVoter(ag_id string, vote_id string, url string, prefs []comsoc.Alternative, options []int) *Voter {
	// constructor of a new voter
	return &Voter{ag_id, vote_id, url, prefs, options}
}

func treatNewBallotResponse(r *http.Response) string {
	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	var resp rad.ResponseNewBallot
	json.Unmarshal(buf.Bytes(), &resp)
	return resp.BallotID
}

func doNewBallotRequest(rule string, ddl string, vots []string, alts int) (res string, err error) {

	// create a new ballot
	req := rad.RequestNewBallot{
		Rule:     rule,
		Deadline: ddl,
		VoterIDs: vots,
		Alts:     alts,
	}

	// sérialisation de la requête
	url := "http://localhost:8080" + "/new_ballot"
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusCreated {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}

	// res returned from treatResponse, than return it to main
	res = treatNewBallotResponse(resp)

	return
}

func NewBallot(rule string, ddl string, vots []string, alts int) string {
	res, err := doNewBallotRequest(rule, ddl, vots, alts)

	if err != nil {
		log.Fatal("error in new ballot:", err.Error())
	} else {
		log.Printf("[POST] succeed create ballot [%s]\n", res)
	}

	return res
}

func (rca *Voter) doVoteRequest() (err error) {

	// create a new Request type and allocate it with agent's own operator and arguments
	req := rad.RequestVote{
		AgentID: rca.ag_id,
		VoteID:  rca.vote_id,
		Prefs:   rca.prefs,
		// options
	}

	// sérialisation de la requête
	url := rca.url + "/vote"
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}

	buf := new(bytes.Buffer)
	buf.ReadFrom(resp.Body)

	log.SetFlags(log.Ldate | log.Ltime)
	if resp.StatusCode == http.StatusOK {
		log.Println(": " + req.AgentID + " vote successfully for " + req.VoteID)
	} else if resp.StatusCode == http.StatusBadRequest {
		log.Println(": " + req.AgentID + " request failed")
		return errors.New("request failed")
	} else if resp.StatusCode == http.StatusForbidden {
		log.Println(": " + req.AgentID + " you have already voted")
		return errors.New("vote exist")
	} else if resp.StatusCode == http.StatusNotImplemented {
		log.Println(": " + req.AgentID + " function has no implemented")
		return errors.New("not implemented")
	} else {
		log.Println(": " + req.AgentID + " vote " + req.VoteID + " has finished")
		return errors.New("time out")
	}

	return
}

func (rca *Voter) Vote() {

	log.Printf("démarrage vote de %s", rca.ag_id)
	err := rca.doVoteRequest()

	if err != nil {
		log.Fatal(rca.ag_id, "vote error:", err.Error())
	} else {
		log.Printf("[POST] agent [%s] succeed vote ballot [%s], preferences = %v\n", rca.ag_id, rca.vote_id, rca.prefs)
	}
}

func treatGetResultResponse(r *http.Response) rad.ResponseResult {
	// create a new Response type, transform from json to object, write response

	buf := new(bytes.Buffer)
	buf.ReadFrom(r.Body)

	var resp rad.ResponseResult
	json.Unmarshal(buf.Bytes(), &resp)
	return resp
}

func doGetResultRequest(vote_id string) (res rad.ResponseResult, err error) {

	req := rad.RequestResult{
		BallotID: vote_id,
	}

	// sérialisation de la requête
	url := "http://localhost:8080" + "/result"
	data, _ := json.Marshal(req)

	// envoi de la requête
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))

	// traitement de la réponse
	if err != nil {
		return
	}
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("[%d] %s", resp.StatusCode, resp.Status)
		return
	}
	if resp.StatusCode == http.StatusTooEarly {
		log.Println(": " + "vote has not finished")
		err = errors.New("Too Early")
		return
	} else if resp.StatusCode == http.StatusNotFound {
		log.Println(": " + "not find this function")
		err = errors.New("Not find")
		return
	}

	// res returned from treatResponse, than return it to main
	res = treatGetResultResponse(resp)

	return
}

func GetResult(vote_id string) (res rad.ResponseResult) {
	res, err := doGetResultRequest(vote_id)

	if err != nil {
		log.Fatal("error in getting result:", err.Error())
	} else {
		log.Printf("[GET] ballot [%s] has winner the candidate %d\n", vote_id, res.Winner)
		log.Printf("[GET] ballot [%s] has a result ranking %d\n", vote_id, res.Ranking)
	}

	return res
}
