package agt

import "gitlab.utc.fr/ia041/ia04-vote/comsoc"

// new_ballot
type RequestNewBallot struct {
	Rule     string   `json:"rule"`
	Deadline string   `json:"deadline"`
	VoterIDs []string `json:"voter-ids"`
	Alts     int      `json:"#alts"`
}

type ResponseNewBallot struct {
	BallotID string `json:"ballot-id"`
}

// vote
type RequestVote struct {
	AgentID string               `json:"agent-id"`
	VoteID  string               `json:"vote-id"`
	Prefs   []comsoc.Alternative `json:"prefs"`
	Options []int                `json:"options"`
}

// result
type RequestResult struct {
	BallotID string `json:"ballot-id"`
}

type ResponseResult struct {
	Winner  comsoc.Alternative   `json:"winner"`
	Ranking []comsoc.Alternative `json:"ranking"`
}
