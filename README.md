# IA04-Vote - Yuqing WANG & Xinghua SHAO
## Introduction
Dans le cadre de ce projet, nous avons réalisé un service de vote (**"un serveur REST"**) basé sur un modèle multi-agent et composé des programmes de serveur et des APIs de support côté client. En implémentant plusieurs méthodes de vote, les agents clients peuvent voter sur ce serveur dans un ballot désigné et obtenir de différents résultats selon la méthode de vote.

En lancant la fonction _main.go_, le serveur est en cours d'écouter des demandes et des requests de client telles que générer un nouveau ballot(**/new_ballot**), voter(**/vote**) et retourner le résultat(**/result**).

On simule également un processus de vote dans la fonction _cmd/all/launch_all_client.go_ en générant 100 votants votent pour 7 candidats. D'abord, on crée un nouveau ballot en lui désignant aléatoirement une méthode de vote et un deadline. Les préférences des votants sont également générées aléatoirement. Après que les votes soient effectués, le résultat de vote sera retourné par le serveur.

Plus de détails sont présentées dans les chapitres suivants.
## Gitlab repository
[https://gitlab.utc.fr/ia041/ia04-vote](https://gitlab.utc.fr/ia041/ia04-vote)
## Installation
### A. Go Build
1. Clonez le code source du projet depuise le site officiel utc gitlab.
```
git clone https://gitlab.utc.fr/ia041/ia04-vote.git
```
2. Modifiez l'adresse IP et le port pour votre serveur ( L'adresse par défaut est **"127.0.0.1:8080"** ).
3. Compilez avec la commande **go build**.
```
go build
```
4. Enfin, exécutez l'exécutable **ia04-vote.exe** dans le répertoire racine du projet
```
./ia04-vote.exe
```
### B. Go Install
Vous pouvez également utiliser la commande **go install** pour lancer ce service.
```go
go install -v gitlab.utc.fr/ia041/ia04-vote@latest
```
Vous trouverez ensuite un fichier exécutable **ia04-vote.exe** dans le dossier **bin** sous le chemin **$GOPATH**.
```go
$GOPATH/bin/ia04-vote.exe
```
Si tous marche bien, vous constaterez le démarrage de serveur en exécutant ce fichier.
![image](https://gitlab.utc.fr/ia041/ia04-vote/raw/main/image/cmd1.png)
## Méthodes de votes implémentées
Nous avons implémenté plusieur méthodes de vote parmi lesquelles les utilisateurs peuvent choisir.  
* approval
* borda
* condorcet
* copeland
* majority
* majority2tours
* stv
## APIs
### 1. Création de ballot de vote : /new_ballot
Cette API permet à l'utilisateur de créer un nouveau ballot de vote avec un méthode de vote spécifié, pour utiliser cette API, l'utilisateur doit fournir 4 paramètres:
1. **rule** de type string. Attention : seulement "approval", "borda", "condorcet", "copeland", "majority", "majority2tours" et "stv" sont actuellement implémentées.
2. **deadline** de type string. Example : "Tue Nov 10 23:00:00 UTC 2009"
3. **voter-ids** de type [string,...]. Ce paramètre représente l'ensemble des votants.
4. **#alts** de type int. Ce paramètre représente le nombre des candidats.
#### L'exemple de création de ballot de vote
![image](https://gitlab.utc.fr/ia041/ia04-vote/raw/main/image/new_ballot_201.png)
Cette image est le résultat renvoyé lorsqu'un ballot de vote est créé avec succès，non seulement il renvoie un code retour 201, mais il renvoie également l'ID du ballot de vote créé. Les ballots de vote sont numérotés à partir de 1.
Cette API peut également retourner deux autres retours de code: 400 et 501
* 400 Bad request(Si les paramètres fournis par l'utilisateur sont incomplets ou au mauvais format)
![image](https://gitlab.utc.fr/ia041/ia04-vote/raw/main/image/new_ballot_400.png)
* 501 Not Implemented(Si l'utilisateur choisit une méthode de vote qui ne fait pas partie des méthodes de vote implémenté)
![image](https://gitlab.utc.fr/ia041/ia04-vote/raw/main/image/new_ballot_501.png)

### 2. Gestion des votants : /vote
Cette API permet d'enregistrer les votes des votants dans les différents ballots de vote. Elle demande également à l'utilisateur de saisir 4 paramètres: 
1. **agent-id** de type string. Ce paramètre représente ID de votants.
2. **vote-id** de type string. Ce paramètre représente ID du ballot de vote pour lequel votant a voté.
3. **prefs** de type [int...]. Ce paramètre représente les préférences de chaque votant.
4. **options** de type [int...]. Ce paramètre représente le seuil. Attention, ce paramètre est facultatif. Lorsque la méthode de vote est approval, ce paramètre doit être renseigné(par exemple [3]). Lorsque la méthode de vote n'est pas approuvée, ce paramètre est vide [].
#### L'exemple de API de vote
![image](https://gitlab.utc.fr/ia041/ia04-vote/raw/main/image/vote_200.png)
Cette image est le résultat renvoyé lorsqu'un vote d'un votant a été pris en compte avec succès，il renvoie seulement un code retour 200.
Cette API peut également retourner quatre autres retours de code: 400, 403, 501 et 503:
* 400 Bad request
* 403 Vote déjà effectué
* 501 Méthod non implémenté
* 503 Dépasser la deadline

### 3. Obtention le résultat de vote
Après avoir collecté les votes des votants, et après la deadline, nous pouvons utiliser cette API pour obtenir les résultats du vote de chaque ballot de vote, nous n'avons besoin que d'un paramètre:
1. **ballot-id** de type string. ID de ballot de vote
#### L'exemple d'obetention le résultat de vote
![image](https://gitlab.utc.fr/ia041/ia04-vote/raw/main/image/result_200.png)
Cette image est le résultat renvoyé lorsque l'utilisateur a obtenu le résultat de vote avec succès，il renvoie non seulement un code retour 200, mais aussi le winner et le ranking.
Pour le ranking, il existe certaines méthodes qui, selon nous, n'ont pas besoin de ranking, telles que condorcet stv, etc.
Cette API peut également retourner quatre autres retours de code: 425 et 404:
* 425 Too early. Faire une demande trop tôt, avant la deadline
* 404 Not Found. On ne peut pas trouver le ballot de vote.

## Fichier exécutable
Le fichier exécutable de server peut afficher des logs, quand il existe des problème, on peut voir facilement.
![image](https://gitlab.utc.fr/ia041/ia04-vote/raw/main/image/cmd.png)
