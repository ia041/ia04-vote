package comsoc

import (
	"errors"
	"sort"
)

type Alternative int           // 候选人
type Profile [][]Alternative   // profile[12][0]：12号选民最喜欢的候选人
type Count map[Alternative]int //

// renvoie l'indice ou se trouve alt dans prefs
// prefs []Alternative 排序方式：最喜欢到最不喜欢
// candidat: 候选人
func Rank(alt Alternative, prefs []Alternative) int {
	for i, candidat := range prefs {
		if alt == candidat {
			return i
		}
	}
	return -1
}

func Ranking(count Count) []Alternative {
	ranking := make([]Alternative, 0)
	numCandidat := len(count)
	type alternatives struct {
		alternative Alternative
		point       int
	}

	tmpList := make([]alternatives, 0)
	// 转换成切片
	for candidat, point := range count {
		tmpList = append(tmpList, alternatives{alternative: candidat, point: point})
	}

	sort.Slice(tmpList, func(i, j int) bool {
		return tmpList[i].point > tmpList[j].point // 降序
	})
	for i := 1; i <= numCandidat; i++ {
		ranking = append(ranking, tmpList[i-1].alternative)
	}
	return ranking
}

// renvoie vrai ssi alt1 est préférée à alt2
func IsPref(alt1, alt2 Alternative, prefs []Alternative) bool {
	if Rank(alt1, prefs) < Rank(alt2, prefs) {
		return true
	} else {
		return false
	}
}

// renvoie les meilleures alternatives pour un décomtpe donné
func MaxCount(count Count) (bestAlts []Alternative) {
	type alternatives struct {
		alternative Alternative
		point       int
	}

	tmpList := make([]alternatives, 0)
	// 转换成切片
	for candidat, point := range count {
		tmpList = append(tmpList, alternatives{alternative: candidat, point: point})
	}

	sort.Slice(tmpList, func(i, j int) bool {
		return tmpList[i].point > tmpList[j].point // 降序
	})

	bestAlts = make([]Alternative, 0)

	bestAlts = append(bestAlts, tmpList[0].alternative)
	for i, candidat := range tmpList {
		if i >= 1 {
			if candidat.point >= count[bestAlts[0]] {
				bestAlts = append(bestAlts, candidat.alternative)
			} else {
				break
			}
		}
	}
	return bestAlts
}

// vérifie le profil donné, par ex. qu'ils sont tous complets et que chaque alternative n'apparaît qu'une seule fois par préférences
func checkProfile(prefs Profile) error {
	check := make(map[Alternative]int)

	for _, candidat := range prefs[0] {
		check[candidat] += 1
	}

	for _, candidats := range prefs {
		for _, candidat := range candidats {
			if check[candidat] != 1 {
				return errors.New("ProfileErrors")
			}
		}
	}
	return nil
}

// vérifie le profil donné, par ex. qu'ils sont tous complets et que chaque alternative de alts apparaît exactement une fois par préférences
func CheckProfileAlternative(prefs Profile, alts []Alternative) error {
	check := make(map[Alternative]int)

	for _, candidat := range alts {
		check[candidat] += 1
	}

	for _, candidats := range prefs {
		if len(candidats) != len(alts) {
			return errors.New("ProfileErrors: longueur de votant est différent de longueur de alts")
		}
		checkNoRepe := make(map[Alternative]int)
		for _, candidat := range candidats {
			checkNoRepe[candidat] += 1
			if checkNoRepe[candidat] > 1 {
				return errors.New("ProfileErrors: alternative répète")
			}
			if check[candidat] != 1 {
				return errors.New("ProfileErrors: alternative est différent de alts")
			}
		}
	}
	return nil
}
