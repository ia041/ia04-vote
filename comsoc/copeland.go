package comsoc

import "errors"

func CopelandSWF(p Profile) (count Count, err error) {
	err = checkProfile(p)

	if err != nil {
		return nil, err
	}
	count = make(Count, 0)
	for _, candidat := range p[0] {
		count[candidat] = 0
	}
	nbAlternative := len(p[0])

	for alt1 := 1; alt1 < nbAlternative; alt1++ {
		for alt2 := alt1 + 1; alt2 <= nbAlternative; alt2++ {
			for _, candidats := range p {
				if IsPref(Alternative(alt1), Alternative(alt2), candidats) == true {
					count[Alternative(alt1)] += 1
				} else {
					count[Alternative(alt2)] -= 1
				}
			}
		}
	}

	return count, nil
}

func CopelandSCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := CopelandSWF(p)
	if err != nil {
		return nil, err
	}
	bestAlts = MaxCount(count)
	// Tie Break
	if len(bestAlts) > 1 {
		N := 10000
		count_tb := TieBreak(bestAlts)
		if count_tb == nil {
			return nil, errors.New("function tie break error")
		}
		for alt, score := range count {
			count[alt] = N*score + count_tb[alt]
		}
		bestAlts = MaxCount(count)
	}
	return bestAlts, err
}
