package comsoc

func CondorcetWinner(p Profile) (bestAlts []Alternative, err error) {
	err = checkProfile(p)

	if err != nil {
		return nil, err
	}

	// slice of alternatives
	altsList := make([]Alternative, 0)
	for _, candidat := range p[0] {
		altsList = append(altsList, candidat)
	}

	// count of alternatives
	numAlternatives := len(altsList)

	// initialiser count
	count := make(Count)
	for _, candidat := range p[0] {
		count[candidat] = 0
	}
	for alt1 := 0; alt1 < numAlternatives-1; alt1++ {
		for alt2 := alt1 + 1; alt2 < numAlternatives; alt2++ {
			var countAlt1, countAlt2 int
			for _, candidats := range p {

				// 强转 int to Alternative
				if IsPref(altsList[alt1], altsList[alt2], candidats) {
					countAlt1 += 1
				} else {
					countAlt2 += 1
				}
			}

			if countAlt1 > countAlt2 {
				count[altsList[alt1]] += 1
			} else if countAlt1 < countAlt2 {
				count[altsList[alt2]] += 1

			}
		}
	}

	bestAlts = make([]Alternative, 0)
	candidatWin := make([]Alternative, 0)
	for candidat, point := range count {
		if point == numAlternatives-1 {
			bestAlts = append(candidatWin, candidat)
		}
	}
	if len(candidatWin) == 1 {
		bestAlts = candidatWin
	}

	return bestAlts, nil

}
