package comsoc

import "errors"

func BordaSWF(p Profile) (count Count, err error) {
	err = checkProfile(p)

	if err != nil {
		return nil, err
	}

	// initialiser count
	count = make(Count)
	for _, candidat := range p[0] {
		count[candidat] = 0
	}

	for _, candidat := range p[0] {
		count[candidat] = 0
	}

	for _, candidats := range p {
		for _, candidat := range candidats {
			count[candidat] += len(candidats) - 1 - Rank(candidat, candidats)
		}
	}

	return count, nil
}

func BordaSCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := BordaSWF(p)
	if err != nil {
		return nil, err
	}
	bestAlts = MaxCount(count)
	// Tie Break
	if len(bestAlts) > 1 {
		N := 10000
		count_tb := TieBreak(bestAlts)
		if count_tb == nil {
			return nil, errors.New("function tie break error")
		}
		for alt, score := range count {
			count[alt] = N*score + count_tb[alt]
		}
		bestAlts = MaxCount(count)
	}
	return bestAlts, err
}
