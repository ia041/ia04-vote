package comsoc

func StvSWF(p Profile) (Count, error) {
	// same as Majority
	count, err := MajoritySWF(p)
	return count, err
}

func STV_SCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := StvSWF(p)
	if err != nil {
		return nil, err
	}
	// sorting count map
	asList := sortCount(count)
	// whether there is "une majorité stricte pour un candidat"
	max_count := asList[0].score
	if max_count > len(p)/2 {
		bestAlts = append(bestAlts, asList[0].alt)
		return
	}
	// n−1 tours
	temp_prof := make(Profile, len(p))
	copy(temp_prof, p)
	for i := 0; i < len(asList)-1; i++ {
		// supprimer le dernier candidat dans profile
		last_alt := asList[len(asList)-1].alt
		deleteLoser(last_alt, temp_prof)
		// obtenir un nouveau "sorted count" en ordre décroissant
		new_count, err := StvSWF(temp_prof)
		if err != nil {
			return nil, err
		}
		asList = sortCount(new_count)
	}
	bestAlts = append(bestAlts, asList[0].alt)
	return
}

func deleteLoser(loser Alternative, p Profile) {
	for i, pref := range p {
		for j, alt := range pref {
			if loser == alt {
				p[i] = append(pref[:j], pref[j+1:]...)
				break
			}
		}
	}
}
