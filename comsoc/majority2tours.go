package comsoc

import (
	"errors"
	"sort"
)

func Majority2ToursSWF(p Profile) (count Count, err error) {
	// same as Majority
	count, err = MajoritySWF(p)
	return
}

func Majority2ToursSCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := Majority2ToursSWF(p)
	if err != nil {
		return nil, err
	}
	// premier tour
	first_count, first_alt, second_alt := FirstTour(count)
	half_ballot := len(p) / 2
	if first_count > half_ballot {
		bestAlts = append(bestAlts, first_alt)
		return
	}
	// deuxieme tour
	bestAlts, err = SecondTour(first_alt, second_alt, p)
	// Tie Break
	if len(bestAlts) > 1 {
		N := 10000
		count_tb := TieBreak(bestAlts)
		if count_tb == nil {
			return nil, errors.New("function tie break error")
		}
		for alt, score := range count {
			count[alt] = N*score + count_tb[alt]
		}
		bestAlts = MaxCount(count)
	}
	return

}

// find the most votes, the alt who has the most votes and the alt who has the second votes
func FirstTour(count Count) (first_count int, first_alt Alternative, second_alt Alternative) {
	asList := sortCount(count)
	first_count = asList[0].score
	first_alt = asList[0].alt
	second_alt = asList[1].alt
	return
}

func SecondTour(first_alt Alternative, second_alt Alternative, p Profile) (bestAlts []Alternative, err error) {
	first_count := 0
	second_count := 0
	for _, pref := range p {
		rank1 := Rank(first_alt, pref)
		rank2 := Rank(second_alt, pref)
		if rank1 == -1 || rank2 == -1 {
			return nil, errors.New("rank error")
		}
		if rank1 < rank2 {
			first_count++
		} else if rank1 > rank2 {
			second_count++
		}
	}
	if first_count == 0 && second_count == 0 {
		return nil, errors.New("rank counting error")
	}
	if first_count > second_count {
		bestAlts = append(bestAlts, first_alt)
	} else if first_count < second_count {
		bestAlts = append(bestAlts, second_alt)
	} else {
		bestAlts = append(bestAlts, first_alt)
		bestAlts = append(bestAlts, second_alt)
	}
	return bestAlts, nil
}

type alt_score struct {
	alt   Alternative
	score int
}
type alt_score_list []alt_score

func (pair alt_score_list) Len() int {
	return len(pair)
}
func (pair alt_score_list) Less(i, j int) bool {
	return pair[i].score < pair[j].score
}
func (pair alt_score_list) Swap(i, j int) {
	pair[i], pair[j] = pair[j], pair[i]
}

func sortCount(count Count) (asList alt_score_list) {
	asList = make(alt_score_list, 0)
	for alt, score := range count {
		asList = append(asList, alt_score{alt, score})

	}
	sort.Sort(sort.Reverse(asList))
	return
}
