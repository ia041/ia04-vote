package comsoc

import (
	"math/rand"
	"time"
)

func TieBreak(alts []Alternative) (count Count) {
	random_scores := generateRandomNumber(0, len(alts), len(alts))
	if random_scores == nil {
		return nil
	}
	count = make(Count)
	for i, v := range alts {
		count[v] = random_scores[i]
	}
	return
}

func generateRandomNumber(start int, end int, count int) (nb_list []int) {
	if end < start || (end-start) < count {
		return nil
	}
	rand.Seed(time.Now().UnixNano())
	for len(nb_list) < count {
		num := rand.Intn(end-start) + start
		exist := false
		for _, v := range nb_list {
			if v == num {
				exist = true
				break
			}
		}
		if !exist {
			nb_list = append(nb_list, num)
		}
	}
	return
}
