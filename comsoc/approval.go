package comsoc

import "errors"

func ApprovalSWF(p Profile, thresholds []int) (count Count, err error) {
	err = checkProfile(p)

	if err != nil {
		return nil, err
	}

	count = make(Count)
	for _, candidat := range p[0] {
		count[candidat] = 0
	}

	for i, candidats := range p {
		for j := 0; j < thresholds[i]; j++ {
			count[candidats[j]] += 1
		}
	}

	return count, nil
}

func ApprovalSCF(p Profile, thresholds []int) (bestAlts []Alternative, err error) {
	count, err := ApprovalSWF(p, thresholds)
	if err != nil {
		return nil, err
	}
	bestAlts = MaxCount(count)
	// Tie Break
	if len(bestAlts) > 1 {
		N := 10000
		count_tb := TieBreak(bestAlts)
		if count_tb == nil {
			return nil, errors.New("function tie break error")
		}
		for alt, score := range count {
			count[alt] = N*score + count_tb[alt]
		}
		bestAlts = MaxCount(count)
	}
	return bestAlts, err
}
