package comsoc

import "errors"

func MajoritySWF(p Profile) (count Count, err error) {
	err = checkProfile(p)

	if err != nil {
		return nil, err
	}

	count = make(Count)

	for _, candidat := range p[0] {
		count[candidat] = 0
	}

	for _, candidats := range p {
		count[candidats[0]] += 1
	}
	return count, nil
}

func MajoritySCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := MajoritySWF(p)
	if err != nil {
		return nil, err
	}
	bestAlts = MaxCount(count)
	// Tie Break
	if len(bestAlts) > 1 {
		N := 10000
		count_tb := TieBreak(bestAlts)
		if count_tb == nil {
			return nil, errors.New("function tie break error")
		}
		for alt, score := range count {
			count[alt] = N*score + count_tb[alt]
		}
		bestAlts = MaxCount(count)
	}
	return bestAlts, err
}
