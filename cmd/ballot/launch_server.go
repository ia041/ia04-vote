package ballot

import (
	"fmt"

	rad "gitlab.utc.fr/ia041/ia04-vote/agt/restserveragent"
)

func main() {
	server := rad.NewRestServerAgent(":8080")
	server.Start()
	fmt.Scanln()
}
