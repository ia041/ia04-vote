package voters

import (
	"fmt"
	"math/rand"
	"time"

	"gitlab.utc.fr/ia041/ia04-vote/agt/restclientagent"
	"gitlab.utc.fr/ia041/ia04-vote/comsoc"
)

func main() {

	nbCandidat := 7
	ag_ids := []string{"ag_id01"}

	// create a new ballot
	bal := restclientagent.NewBallot("majority", "Mon Nov 7 23:59:00 UTC 2022", ag_ids, nbCandidat)

	// simulate random preference for ag_id01
	permutation := rand.Perm(nbCandidat)
	prefs := make([]comsoc.Alternative, 0, 7)
	for i := range permutation {
		permutation[i] += 1
		prefs[i] = comsoc.Alternative(permutation[i])
	}

	// vote
	ag := restclientagent.NewVoter("ag_id01", bal, "http://localhost:8080", prefs, nil)
	ag.Vote()
	time.Sleep(60 * time.Second)
	// result
	restclientagent.GetResult(bal)
	fmt.Scanln()
}
