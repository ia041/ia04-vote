package main

import (
	"fmt"
	"log"
	"math/rand"
	"time"

	rca "gitlab.utc.fr/ia041/ia04-vote/agt/restclientagent"
	rsa "gitlab.utc.fr/ia041/ia04-vote/agt/restserveragent"
	com "gitlab.utc.fr/ia041/ia04-vote/comsoc"
)

func main() {
	const nbVoter = 100
	const nbCandidat = 7
	const url1 = ":8000"
	const url2 = "http://localhost:8000"

	methds := [...]string{"majority", "borda", "approval", "condorcet", "copeland", "majority2tours", "stv"}
	rand.Seed(time.Now().UnixNano())
	methd := methds[rand.Intn(7)]

	ddl := "Mon Nov 7 16:44:00 UTC 2022"

	clVots := make([]rca.Voter, 0, nbVoter)
	servAgt := rsa.NewRestServerAgent(url1)

	log.Println("démarrage du serveur")
	// launch the serveur
	go servAgt.Start()

	log.Println("démarrage du premier ballot")
	// create a new ballot
	ag_ids := []string{"ag_id01", "ag_iid02", "ag_id03"}
	bal_01 := rca.NewBallot(methd, ddl, ag_ids, nbCandidat)

	log.Println("démarrage des clients")
	// create 100 agents and append to clVots table
	for i := 0; i < nbVoter; i++ {
		// simulate random preference
		permutation := rand.Perm(nbCandidat)
		prefs := make([]com.Alternative, 7)
		for j := range permutation {
			permutation[j] += 1
			prefs[j] = com.Alternative(permutation[j])
		}

		id := fmt.Sprintf("ag_id%02d", i)
		agt := rca.NewVoter(id, bal_01, url2, prefs, nil)
		clVots = append(clVots, *agt)
	}

	// 100 agents vote
	for _, agt := range clVots {
		func(agt rca.Voter) {
			go agt.Vote()
		}(agt)
	}

	time.Sleep(60 * time.Second)
	// get result
	rca.GetResult(bal_01)

	fmt.Scanln()
}
