package main

import (
	"fmt"

	rad "gitlab.utc.fr/ia041/ia04-vote/agt/restserveragent"
)

func main() {
	server := rad.NewRestServerAgent("127.0.0.1:8080")
	server.Start()
	fmt.Scanln()
}
